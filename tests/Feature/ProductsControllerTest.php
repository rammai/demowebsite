<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductsControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_product_can_added_successfully()
    {
        $this->withoutExceptionHandling();
        $response = $this->post('/products', [
            "sku" => "test001",
            "name" => "Test Product"
        ]);

        $response->assertOk();
        $this->assertCount(1, Product::all());
    }

    /** @test */
    public function a_sku_is_required()
    {
        //$this->withoutExceptionHandling();
        $response = $this->post('/products', [
            "sku" => "",
            "name" => "Test Product"
        ]);

        $response->assertSessionHasErrors("sku");
    }

    /** @test */
    public function a_name_is_required()
    {
        //$this->withoutExceptionHandling();
        $response = $this->post('/products', [
            "sku" => "test001",
            "name" => ""
        ]);

        $response->assertSessionHasErrors("name");
    }

    /** @test */
    public function product_updated_successfully()
    {
        $this->withoutExceptionHandling();
        $this->post('/products', [
            "sku" => "test001",
            "name" => "Test Product"
        ]);

        $product = Product::first();

        $response = $this->patch('/products/' . $product->id, [
            "sku" => "newSku",
            "name" => "New Name"
        ]);

        $this->assertEquals("newSku", Product::first()->sku);
        $this->assertEquals("New Name", Product::first()->name);
    }
}
